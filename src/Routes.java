import java.io.*;
import java.util.*;

public class Routes {
    public static ArrayList<MapStops.EdgeTrip> inputs;
    public static Map<String, ArrayList<MapStops.EdgeTrip>> sortedTimes;

    public Routes(File stop_times) throws IOException {
        inputs = new ArrayList<>();
        sortedTimes = new TreeMap<>();
        readStopTimes(stop_times);
        sortByTime();
    }

    public void readStopTimes(File stop_times) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(stop_times));
        String str;
        int count = 0;
        try {
            while ((str = br.readLine()) != null) {
                String[] strln = str.split(",");
                int emptyInt = -1;
                float emptyFloat = -1;
                if (count != 0) {
                    int trip_id = emptyInt;
                    int stop_id = emptyInt;
                    int stop_sequence = emptyInt;
                    int stop_headsign = emptyInt;
                    int pickup_type = emptyInt;
                    int drop_off_type = emptyInt;
                    float shape_dist_traveled = emptyFloat;

                    String arr_time = strln[1];
                    String dep_time = strln[2];

                    if (!strln[0].equals("")) {
                        trip_id = Integer.parseInt(strln[0]);
                    }
                    if (!strln[3].equals("")) {
                        stop_id = Integer.parseInt(strln[3]);
                    }
                    if (!strln[4].equals("")) {
                        stop_sequence = Integer.parseInt(strln[4]);
                    }
                    if (!strln[5].equals("")) {
                        stop_headsign = Integer.parseInt(strln[5]);
                    }
                    if (!strln[6].equals("")) {
                        pickup_type = Integer.parseInt(strln[6]);
                    }
                    if (!strln[7].equals("")) {
                        drop_off_type = Integer.parseInt(strln[7]);
                    }
                    if ((strln.length == 9) && !strln[8].equals("")) {
                        shape_dist_traveled = Float.parseFloat(strln[8]);
                    }
                    if (isValidTime(arr_time) && isValidTime(dep_time)) {
                        inputs.add(new MapStops.EdgeTrip(trip_id, arr_time, dep_time, stop_id, stop_sequence,
                                stop_headsign, pickup_type, drop_off_type, shape_dist_traveled));
                    }
                }
                count++;
            }
        } catch (Exception e) {
            System.out.println("Error reading files");
        }
    }

    public static boolean isValidTime(String time) {
        final int MAX_HR = 23;
        final int MAX_MIN = 59;
        final int MAX_SEC = 59;

        String time_linked = time.replaceAll("\\s", "");

        String[] timeArr = time_linked.split(":");
        int hours;
        int minutes;
        int seconds;
        try {
            hours = Integer.parseInt(timeArr[0]);
            minutes = Integer.parseInt(timeArr[1]);
            seconds = Integer.parseInt(timeArr[2]);
        } catch (Exception e) {
            return false;
        }
        return (hours <= MAX_HR) && (minutes <= MAX_MIN) && (seconds <= MAX_SEC);
    }

    public static void sortByTime() {
        for (MapStops.EdgeTrip input : inputs) {
            String line = input.arrival_time;
            line = line.replace(" ", "0");
            sortedTimes.computeIfAbsent(line, k -> new ArrayList<>()).add(input);
        }
    }

    public static void sortTripsBasedOnID(String time) {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            MapStops.EdgeTrip[] list;
            MapStops.EdgeTrip temp;
            for (int i = 0; i < sortedTimes.get(time).size() - 1; i++) {
                int cur = sortedTimes.get(time).get(i).trip_id;
                int next = sortedTimes.get(time).get(i+1).trip_id;
                if (cur > next) {
                    temp = sortedTimes.get(time).get(i);
                    sortedTimes.get(time).set(i, sortedTimes.get(time).get(i+1));
                    sortedTimes.get(time).set(i+1, temp);
                    sorted = false;
                }
            }
        }
    }

}
