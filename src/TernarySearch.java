import java.util.ArrayList;

public class TernarySearch {
    private Node root;
    private ArrayList<String> list;

    public TernarySearch() {
        root = null;
    }

    public void clear() {
        root = null;
    }

    public void insert(String str) {
        root = insert(root, str.toCharArray(), 0);
    }

    public Node insert(Node nodeRoot, char[] str, int index) {
        if (nodeRoot == null)
            nodeRoot = new Node(str[index]);

        if (str[index] < nodeRoot.chr)
            nodeRoot.left = insert(nodeRoot.left, str, index);
        else if (str[index] > nodeRoot.chr)
            nodeRoot.right = insert(nodeRoot.right, str, index);
        else {
            if (index + 1 < str.length)
                nodeRoot.centre = insert(nodeRoot.centre, str, index + 1);
            else
                nodeRoot.isLeaf = true;
        }
        return nodeRoot;
    }

    public void delete(String str) {
        delete(root, str.toCharArray(), 0);
    }

    private void delete(Node nodeRoot, char[] str, int index) {
        if (nodeRoot == null)
            return;

        if (str[index] < nodeRoot.chr)
            delete(nodeRoot.left, str, index);
        else if (str[index] > nodeRoot.chr)
            delete(nodeRoot.right, str, index);
        else {
            if (nodeRoot.isLeaf && index == str.length - 1)
                nodeRoot.isLeaf = false;

            else if (index + 1 < str.length)
                delete(nodeRoot.centre, str, index + 1);
        }
    }

    public String[] search(String str) {
        String[] emptyStringArr = new String[0];
        try {
            if (str == null) {
                return emptyStringArr;
            }
        } catch (Exception e) {
            System.out.println("Null Encountered");
        }
        try {
            if (str.charAt(0) == ' ') {
                return emptyStringArr;
            }

        } catch (Exception e) {
            System.out.println("Error, please enter a valid string");
        }

        StringBuilder sb = new StringBuilder();

        Node startNode = getNode(root, str.toCharArray(), 0);
        searchAllStrings(startNode, "", sb, str);
        if (sb.length() < 1) {
            System.out.println("No Match Found");
            return emptyStringArr;
        }
        return sb.toString().split("\n");
    }

    public String toString() {
        list = new ArrayList<>();
        traverseNodes(root, "");
        return "\nTernary Search Tree : " + list;
    }

    private void traverseNodes(Node node, String str) {
        if (node != null) {
            traverseNodes(node.left, str);

            str = str + node.chr;
            if (node.isLeaf)
                list.add(str);

            traverseNodes(node.centre, str);
            str = str.substring(0, str.length() - 1);

            traverseNodes(node.right, str);
        }
    }

    private Node getNode(Node node, char[] str, int index) {
        if (node == null)
            return null;
        if (str[index] < node.chr)
            return getNode(node.left, str, index);
        else if (str[index] > node.chr)
            return getNode(node.right, str, index);
        else {
            if (index == str.length - 1)
                return node;
            else
                return getNode(node.centre, str, index + 1);
        }
    }

    private void searchAllStrings(Node node, String str, StringBuilder sb, String source) {
        if (node != null) {
            searchAllStrings(node.left, str, sb, source);
            str = str + node.chr;
            if (node.isLeaf) {
                if (source.length() == 1) {
                    if (source.equals(str.substring(0, 1)))
                        sb.append(source).append(str.substring(1)).append("\n");
                } else
                    sb.append(source).append(str.substring(1)).append("\n");

            }
            searchAllStrings(node.centre, str, sb, source);
            str = str.substring(0, str.length() - 1);
            searchAllStrings(node.right, str, sb, source);
        }
    }

    public static class Node {
        char chr;
        boolean isLeaf;
        Node left, centre, right;

        /** Constructor **/
        public Node(char chr) {
            this.chr = chr;
            this.isLeaf = false;
            this.left = null;
            this.centre = null;
            this.right = null;
        }
    }
}