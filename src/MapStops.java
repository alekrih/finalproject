import java.io.*;
import java.util.*;

public class MapStops {
    public static HashMap<Integer, ArrayList<EdgeCon>> adjHash;
    public static HashMap<String, EdgeStop> nameHash;

    private static void initConnections() {
        adjHash = new HashMap<>();
        nameHash = new HashMap<>();
    }

    public MapStops() {
        initConnections();
    }

    public MapStops(File stops, File transfers) throws IOException {
        initConnections();
        readStops(stops);
        readTransfers(transfers);
    }

    private static void readTransfers(File transfer) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(transfer));
        String str;
        int lineCount = 0;
        try {
            while ( (str = br.readLine() ) != null) {
                String[] strLn = str.split(",");
                if (lineCount != 0) {

                    int emptyCode = -1;
                    int from_stop_id = (strLn[0].equals("") || strLn[0].equals(" ")) ? emptyCode
                            : Integer.parseInt(strLn[0]);
                    int to_stop_id = (strLn[1].equals("") || strLn[1].equals(" ")) ? emptyCode
                            : Integer.parseInt(strLn[1]);
                    int transType = (strLn[2].equals("") || strLn[2].equals(" ")) ? emptyCode
                            : Integer.parseInt(strLn[2]);
                    double cost;
                    switch (transType) {
                        case 0 -> {
                            cost = 2;
                            findLinks(from_stop_id, to_stop_id, cost);
                        }
                        case 2 -> {
                            double minTrans = Double.parseDouble(strLn[3]);
                            cost = minTrans / 100;
                            findLinks(from_stop_id, to_stop_id, cost);
                        }
                        default -> throw new Exception("invalid transfer type at line" + lineCount + "\n" + Arrays.toString(strLn));
                    }
                }
                lineCount++;
            }
        } catch (Exception e) {
            System.out.println("Error reading files.");
        }
        br.close();
    }

    public static void findLinks(int from_stop_id, int to_stop_id, double cost) {
        adjHash.computeIfAbsent(from_stop_id, k -> new ArrayList<>());
        adjHash.computeIfAbsent(to_stop_id, k -> new ArrayList<>());

        adjHash.get(from_stop_id).add(new EdgeCon(to_stop_id, cost));
    }

    private static void readStops(File stops) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(stops));
        String st;
        int count = 0;
        int emptyInt = -1;
        double emptyDouble = -1.0;
        String empty = "";
        try {
            while ((st = br.readLine()) != null) {
                String[] line = st.split(",");
                if (count != 0) {
                    int stop_id = (line[0].equals("") || line[0].equals(" ")) ? emptyInt : Integer.parseInt(line[0]);

                    int stop_code = (line[1].equals("") || line[1].equals(" ")) ? emptyInt : Integer.parseInt(line[1]);

                    String stop_name = (line[2].equals("") || line[2].equals(" ")) ? empty
                            : cleanUp(line[2]);
                    FinalProject.TST.insert(stop_name);
                    String stop_desc = (line[3].equals("") || line[3].equals(" ")) ? empty : line[3];

                    double stop_lat = (line[4].equals("") || line[4].equals(" ")) ? emptyDouble
                            : Double.parseDouble(line[4]);

                    double stop_lon = (line[5].equals("") || line[5].equals(" ")) ? emptyDouble
                            : Double.parseDouble(line[5]);

                    String zone_id = (line[6].equals("") || line[6].equals(" ")) ? empty : line[6];

                    String stop_url = (line[7].equals("") || line[7].equals(" ")) ? empty : line[7];

                    int location_type = (line[8].equals("") || line[8].equals(" ")) ? emptyInt
                            : Integer.parseInt(line[8]);

                    String parent = (line.length == 9) ? empty : line[9];

                    addStop(new EdgeStop(stop_id, stop_code, stop_name, stop_desc, stop_lat, stop_lon, zone_id, stop_url,
                            location_type, parent));
                }
                count++;
            }
        } catch (Exception e) {
            System.out.println("Error reading files.");
        }
        br.close();
    }

    public static void addStop(EdgeStop edgeStop) {
        adjHash.put(edgeStop.stop_id, new ArrayList<>());
        nameHash.put(edgeStop.stop_name, edgeStop);
    }

    public static String cleanUp(String name) {

        int length = 2;
        int fsLength = 8;

        String temp = name.substring(0, length).strip().toUpperCase();
        String tempFS = name.substring(0, fsLength).strip().toUpperCase();

        if (temp.equals("WB") || temp.equals("NB") || temp.equals("SB") || temp.equals("EB")) {
            String end = name.substring(length + 1);
            String bgn = name.substring(0, length);
            String cleanUpStr = end.concat(" ").concat(bgn);
            return cleanUp(cleanUpStr);
        }
        if (tempFS.equals("FLAGSTOP")) {
            String lastPart = name.substring(fsLength + 1);
            String firstPart = name.substring(0, fsLength);
            String meaningfulStr = lastPart.concat(" ").concat(firstPart);
            return cleanUp(meaningfulStr);
        } else
            return name;
    }

    private static double minCost;

    public static double getMinCost() {
        return minCost;
    }

    public static boolean s_ID_Valid(int stop_id) {
        return adjHash.containsKey(stop_id);
    }

    public static void shortestPathPrint(int from, int to) {
        ArrayList<Integer> path = MapStops.getShortestPath(from, to);
        double cost = MapStops.getMinCost();

        if (cost == Double.POSITIVE_INFINITY) {
            System.out.println("No route found from from " + from + " to " + to);
        } else if (cost == Double.NEGATIVE_INFINITY) {
            System.out.println("Error: Same bus stop");
        } else if (cost == -1.0) {
            System.out.println("Invalid input");
        } else {
            System.out.println("The total cost from " + from + " to " + to + " is: " + cost);
            if (path != null) {
                for (int i = 0; i < path.size(); i++) {
                    System.out.print(path.get(i));
                    if (i != path.size() - 1) {
                        System.out.print(" -> ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static ArrayList<Integer> getShortestPath(int from, int to) {
        if (s_ID_Valid(from) && s_ID_Valid(to)) {
            if (from == to) {
                minCost = Double.NEGATIVE_INFINITY;
                return null;
            }
            HashMap<Integer, Double> distTo = new HashMap<>(adjHash.size());
            HashMap<Integer, Integer> previous = new HashMap<>(adjHash.size());
            HashSet<Integer> visited = new HashSet<>(adjHash.size());

            for (int key : adjHash.keySet()) {
                distTo.put(key, Double.POSITIVE_INFINITY);
                previous.put(key, Integer.MAX_VALUE);
                visited.add(key);
            }

            distTo.put(from, 0.0);

            while (!visited.isEmpty()) {
                int cur = Integer.MAX_VALUE;
                double min = Double.POSITIVE_INFINITY;
                for (int val : visited) {
                    double newVal = distTo.get(val);
                    if (newVal < min) {
                        min = newVal;
                        cur = val;
                    }
                }
                if (cur == Integer.MAX_VALUE) {
                    break;
                }
                visited.remove(cur);

                if (cur == to) {
                    break;
                }

                ArrayList<EdgeCon> adjacent = adjHash.get(cur);

                if (adjacent != null) {
                    for (EdgeCon adj : adjacent) {
                        if (adj.cost != Double.POSITIVE_INFINITY && distTo.get(cur) != null) {
                            double dist = distTo.get(cur) + adj.cost;
                            if (distTo.get(adj.stop_id) > dist) {
                                distTo.put(adj.stop_id, dist);
                                previous.put(adj.stop_id, cur);
                            }
                        }
                    }
                }
            }

            ArrayList<Integer> shortestP = new ArrayList<>();
            int stop = to;
            if (previous.get(stop) != null) {
                if (previous.get(stop) != Integer.MAX_VALUE) {
                    while (stop != Integer.MAX_VALUE) {
                        shortestP.add(0, stop);
                        stop = previous.get(stop);
                    }
                }
            }

            if (distTo.get(to) != null)
                minCost = distTo.get(to);

            return shortestP;
        }
        if (!s_ID_Valid(from)) {
            System.out.println("invalid from stop id " + from);
        }
        if (!s_ID_Valid(to)) {
            System.out.println("invalid to stop id " + to);
        }
        minCost = -1.0;
        return null;
    }

    public static class EdgeCon {
        public int stop_id;
        public double cost;

        public EdgeCon(int stop_id, double cost) {
            this.stop_id = stop_id;
            this.cost = cost;
        }
    }

    public static class EdgeStop {
        public int stop_id;
        public int stop_code;
        public String stop_name;
        public String stop_desc;
        public double stop_lat;
        public double stop_lon;
        public String zone_id;
        public String stop_url;
        public int location_type;
        public String parent_station;

        public EdgeStop(int stop_id, int stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon,
                        String zone_id, String stop_url, int location_type, String parent_station) {
            this.stop_id = stop_id;
            this.stop_code = stop_code;
            this.stop_name = stop_name;
            this.stop_desc = stop_desc;
            this.stop_lat = stop_lat;
            this.stop_lon = stop_lon;
            this.zone_id = zone_id;
            this.stop_url = stop_url;
            this.location_type = location_type;
            this.parent_station = parent_station;
        }

        public void printStopDetails() {
            String id = ", Stop ID - ";
            String code = ", Stop Code - ";
            String name = "Name - ";
            String dest = ", Destination - ";
            String lat = ", Latitude - ";
            String lon = ", Longitude - ";
            String z_id = ", Zone ID - ";
            String url = ", Zone URL - ";
            String type = ", Location Type - ";
            String stat = ", Parent Station - ";

            System.out.println(name + stop_name + id + stop_id + code + stop_code + dest
                    + stop_desc + lat + stop_lat + lon + stop_lon + z_id + zone_id + url
                    + stop_url + type + location_type + stat + parent_station + "\n");
        }
    }

    public static class EdgeTrip {
        int trip_id;
        String arrival_time;
        String departure_time;
        int stop_id;
        int stop_sequence;
        int stop_headsign;
        int pickup_type;
        int drop_off_type;
        float shape_dist_traveled;

        public EdgeTrip(int trip_id, String arrival_time, String departure_time, int stop_id, int stop_sequence,
                        int stop_headsign, int pickup_type, int drop_off_type, float shape_dist_traveled) {
            this.trip_id = trip_id;
            this.arrival_time = arrival_time;
            this.departure_time = departure_time;
            this.stop_id = stop_id;
            this.stop_sequence = stop_sequence;
            this.stop_headsign = stop_headsign;
            this.pickup_type = pickup_type;
            this.drop_off_type = drop_off_type;
            this.shape_dist_traveled = shape_dist_traveled;
        }

        public void printTripDetails() {
            String id = "ID - ";
            String arr = " Arrival Time - ";
            String dep = " Departure Time - ";
            String stopid = " Stop ID - ";
            String seq = " Stop Sequence - ";
            String hs = " Stop Head-sign - ";
            String p_type = " Pickup Type - ";
            String do_type = " Drop-off Type - ";
            String shp = " Shape Distance Travelled - ";
            System.out.println(id + trip_id + arr + arrival_time + dep + departure_time
                    + stopid + this.stop_id + seq + stop_sequence + hs + stop_headsign + p_type
                    + pickup_type + do_type + drop_off_type + shp + shape_dist_traveled);
        }
    }
}
