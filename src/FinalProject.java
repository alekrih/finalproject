import java.util.*;
import java.io.*;

public class FinalProject {
    public static final Scanner input = new Scanner(System.in);

    public static MapStops stopsAndTransfers;
    public static Routes stopTimeRoutes;

    public static TernarySearch TST = new TernarySearch();

    public static void initInputs(File stops, File stop_times, File transfers) throws IOException {
        stopsAndTransfers = new MapStops(stops, transfers);
        stopTimeRoutes = new Routes(stop_times);

        for (int i = 1; i < stopTimeRoutes.inputs.size(); i++) {
            MapStops.EdgeTrip edge1 = stopTimeRoutes.inputs.get(i - 1);
            MapStops.EdgeTrip edge2 = stopTimeRoutes.inputs.get(i);
            int cost = 1;
            if (edge1.trip_id == edge2.trip_id) {
                MapStops.findLinks(edge1.stop_id, edge2.stop_id, cost);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("""

                BBBBBBBBBBBBBBBBB   UUUUUUUU     UUUUUUUU   SSSSSSSSSSSSSSS LLLLLLLLLLL             IIIIIIIIIINNNNNNNN        NNNNNNNNKKKKKKKKK    KKKKKKK
                B::::::::::::::::B  U::::::U     U::::::U SS:::::::::::::::SL:::::::::L             I::::::::IN:::::::N       N::::::NK:::::::K    K:::::K
                B::::::BBBBBB:::::B U::::::U     U::::::US:::::SSSSSS::::::SL:::::::::L             I::::::::IN::::::::N      N::::::NK:::::::K    K:::::K
                BB:::::B     B:::::BUU:::::U     U:::::UUS:::::S     SSSSSSSLL:::::::LL             II::::::IIN:::::::::N     N::::::NK:::::::K   K::::::K
                  B::::B     B:::::B U:::::U     U:::::U S:::::S              L:::::L                 I::::I  N::::::::::N    N::::::NKK::::::K  K:::::KKK
                  B::::B     B:::::B U:::::D     D:::::U S:::::S              L:::::L                 I::::I  N:::::::::::N   N::::::N  K:::::K K:::::K  \s
                  B::::BBBBBB:::::B  U:::::D     D:::::U  S::::SSSS           L:::::L                 I::::I  N:::::::N::::N  N::::::N  K::::::K:::::K   \s
                  B:::::::::::::BB   U:::::D     D:::::U   SS::::::SSSSS      L:::::L                 I::::I  N::::::N N::::N N::::::N  K:::::::::::K    \s
                  B::::BBBBBB:::::B  U:::::D     D:::::U     SSS::::::::SS    L:::::L                 I::::I  N::::::N  N::::N:::::::N  K:::::::::::K    \s
                  B::::B     B:::::B U:::::D     D:::::U        SSSSSS::::S   L:::::L                 I::::I  N::::::N   N:::::::::::N  K::::::K:::::K   \s
                  B::::B     B:::::B U:::::D     D:::::U             S:::::S  L:::::L                 I::::I  N::::::N    N::::::::::N  K:::::K K:::::K  \s
                  B::::B     B:::::B U::::::U   U::::::U             S:::::S  L:::::L         LLLLLL  I::::I  N::::::N     N:::::::::NKK::::::K  K:::::KKK
                BB:::::BBBBBB::::::B U:::::::UUU:::::::U SSSSSSS     S:::::SLL:::::::LLLLLLLLL:::::LII::::::IIN::::::N      N::::::::NK:::::::K   K::::::K
                B:::::::::::::::::B   UU:::::::::::::UU  S::::::SSSSSS:::::SL::::::::::::::::::::::LI::::::::IN::::::N       N:::::::NK:::::::K    K:::::K
                B::::::::::::::::B      UU:::::::::UU    S:::::::::::::::SS L::::::::::::::::::::::LI::::::::IN::::::N        N::::::NK:::::::K    K:::::K
                BBBBBBBBBBBBBBBBB         UUUUUUUUU       SSSSSSSSSSSSSSS   LLLLLLLLLLLLLLLLLLLLLLLLIIIIIIIIIINNNNNNNN         NNNNNNNKKKKKKKKK    KKKKKKK
                """);

        System.out.println("Welcome to the Vancouver bus management system.");
        System.out.println("Loading...");
        String stopTimes = "./src/input files/stop_times.txt";
        File stop_times = new File(stopTimes);

        String stopsFile = "./src/input files/stops.txt";
        File stops = new File(stopsFile);

        String transfersFile = "./src/input files/transfers.txt";
        File transfers = new File(transfersFile);

        initInputs(stops, stop_times, transfers);

        System.out.println("Thank you for your patience!");
        boolean finished = false;
        while (!finished) {
            System.out.print("""

                    Please select one of the following options or type 'exit' to quit the program:
                    1 - Find the shortest path between two bus stops
                    2 - Find information on a specific stop
                    3 - Find all Routes with a given arrival time
                    -\s""");
            var choice = input.next();
            input.nextLine();
            boolean isInt = false;
            int val = 0;
            try {
                val = Integer.parseInt(choice);
                if ( val != 0) isInt = true;
            } catch (NumberFormatException e) {
                System.out.println();
            }
            if (choice.equals("exit")) {
                finished = true;
            } else if (isInt) {
                if (val > 0 && val < 4) {
                    switch (val) {
                        case 1 -> {
                            int start = 0;
                            int end = 0;
                            boolean accepted = false;
                            while (!accepted) {
                                System.out.println("\nPlease enter the starting location:");
                                if (input.hasNextInt()) {
                                    start = input.nextInt();
                                    input.nextLine();
                                    accepted = true;
                                } else {
                                    input.nextLine();
                                    System.out.println("Error: Please enter a valid number");
                                }
                            }
                            accepted = false;
                            while (!accepted) {
                                System.out.println("Please enter the final location:");
                                if (input.hasNextInt()) {
                                    end = input.nextInt();
                                    input.nextLine();
                                    accepted = true;
                                } else {
                                    input.nextLine();
                                    System.out.println("Error: Please enter a valid number");
                                }
                            }
                            if (MapStops.s_ID_Valid(start) && MapStops.s_ID_Valid(end)) {
                                MapStops.shortestPathPrint(start, end);
                            } else {
                                System.out.println("Error: No bus-stops found.");
                            }
                        }
                        case 2 -> {
                            System.out.println("\nPlease enter the stop name:");
                            String stop = input.nextLine();
                            String searchInput = stop.toUpperCase();
                            String[] output = TST.search(searchInput);
                            if (output == null) {
                                String errorMessage = "Sorry, There doesn't seem to be any stops by that name.";
                                System.out.println(errorMessage);
                            } else {
                                for (String str : output) {
                                    MapStops.EdgeStop curStop = MapStops.nameHash.get(str);
                                    if ( curStop != null ) curStop.printStopDetails();
                                }
                            }
                        }
                        case 3 -> {
                            System.out.println("\nPlease enter your departure time in the format (HH:MM:SS):");
                            String departureTime = input.nextLine();
                            if ( Routes.sortedTimes.get(departureTime) != null ) {
                                Routes.sortTripsBasedOnID(departureTime);
                                for (MapStops.EdgeTrip curTrip : Routes.sortedTimes.get(departureTime)) {
                                    curTrip.printTripDetails();
                                    System.out.println();
                                }
                            } else {
                                System.out.println("No routes found, please ensure you entered the time in the correct format, eg. 09:11:29");
                            }
                        }
                        default -> {
                        }
                    }
                }
            }
        }
        System.out.print("Thank you for using the Vancouver bus management system. Have a nice day!");
    }
}
